<?php

// 1.

$numbers = array(68, 70, 72, 58, 60, 79, 82, 73, 75, 77, 73, 58, 63, 79, 78,
68, 72, 73, 80, 79, 68, 72, 75, 77, 73, 78, 82, 85, 89, 83);
$min = $numbers[0];

foreach ($numbers as $value) 
{
	if ($min > $value) 
	{
		$min = $value;
	}
}
echo $min;

echo '<br>';

$max = $numbers[0];

foreach ($numbers as $value) 
{
	if ($max < $value) 
	{
		$max = $value;
	}
}
echo $max;

echo '<br>';

// 2.

$elements = [54, 6, 7, 2, 34, 0, 65];

echo $elements[0] . '<br>';

echo ($elements[count($elements) - 1]). "<br>";

// 3.

$numbers = array(68, 70, 72, 58, 60, 79, 82, 73, 75, 77, 73, 58, 63, 79, 78,
68, 72, 73, 80, 79, 68, 72, 75, 77, 73, 78, 82, 85, 89, 83);
$parni = 0;

foreach ($numbers as $value) 
{
	if($value % 2 == 0)
	{
		$parni += $value;
	}
}
echo $parni;

echo '<br>';

// 4.

$numbers = array(68, 70, 72, 58, 60, 79, 82, 73, 75, 77, 73, 58, 63, 79, 78,
68, 72, 73, 80, 79, 68, 72, 75, 77, 73, 78, 82, 85, 89, 83);
$proizvod = 1;
foreach ($numbers as $value) 
{
	if ($value + $value > 152) 
	{
		$proizvod *= $value;
	}
}
echo $proizvod;
echo '<br>';

// 5.

$array = ['ab', 'eee', 'abababa', 'h', 'ytyty', 'jdwygdygd', 'ter'];
$najdolg = $array[0];

foreach ($array as $key) 
{
	if (strlen($key) > strlen($najdolg)) 
	{
		$najdolg = $key;
	}
}

echo $najdolg;
echo '<br>';

// 6.

$array = ['ab', 'eee', 'abababa', 'h', 'ytyty', 'jdwygdygd', 'ter'];
$najkratok = $array[0];

foreach ($array as $key) 
{
	if (strlen($key) < strlen($najkratok)) 
	{
		$najkratok = $key;
	}
}

echo $najkratok;
echo '<br>';

// 7.

$numbers = array(68, 70, 72, 58, 60, 79, 82, 73, 75, 77, 73, 58, 63, 79, 78,
68, 72, 73, 80, 79, 68, 72, 75, 77, 73, 78, 82, 85, 89, 83);
$suma = 0;
$prosek;

foreach ($numbers as $value) 
{
	$suma += $value;
}
$prosek = $suma / count($numbers);
echo $prosek;
echo '<br>';

// 8.

$array = 
[
	"Peter" => 3,
	"Ben" => 4,
	"Joe" => 5,
	"John" => 6,
	"Sofia" => 7,
	"Chris" => 8
];
$brojnaelementi = count($array);

foreach ($array as $key => $value) 
{
	if($value == $brojnaelementi)
	{
		echo $key;
	}	
}

echo '<br>';

// 9.

$array = 
[
	"Peter" => 3,
	"Ben" => 4,
	"Joe" => 5,
	"John" => 6,
	"Sofia" => 7,
	"Chris" => 8
];
$counter =1;

foreach ($array as $key => $value) 
{
	if($counter == 5)
	{
		echo $key;
	}
	$counter++;
}
echo '<br>';



?>